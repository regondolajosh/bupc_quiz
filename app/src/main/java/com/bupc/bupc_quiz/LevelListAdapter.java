package com.bupc.bupc_quiz;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bupc.bupc_quiz.fragments.QuestionsFragment;

public class LevelListAdapter extends BaseAdapter {

    public static final String TAG = LevelListAdapter.class.getSimpleName();
    private MainActivity mMainActivity;
    private Context mContext;
    private LayoutInflater mInflater;
    private ViewHolder holder = null;
    private int mLevelUnlocked = 0;

    public LevelListAdapter(Activity activity, Context context, int mLevelUnlocked) {
        this.mMainActivity = (MainActivity) activity;
        this.mContext = context;
        this.mLevelUnlocked = mLevelUnlocked;
    }

    @Override
    public int getCount() {
        return 50;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int position, View view, ViewGroup viewGroup) {
        if (view == null) {
            mInflater = (LayoutInflater) mContext.getSystemService(mContext.LAYOUT_INFLATER_SERVICE);
            view = mInflater.inflate(R.layout.level_item, viewGroup, false);
            holder = new ViewHolder(view);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        holder.levelNum.setText(String.valueOf(position + 1));

        if ((position + 1) > mLevelUnlocked) {
            holder.lock.setVisibility(View.VISIBLE);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AppClass.getInstance().toaster("Level is still locked!");
                }
            });

        } else {
            holder.lock.setVisibility(View.GONE);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    Prefs.setLevel(position + 1);
                    DataHolder.getInstance().setLevel(position + 1);
                    mMainActivity.switchFragment(new QuestionsFragment(), "questions");
                }
            });
        }

        return view;
    }


    public class ViewHolder {
        private TextView levelNum;
        private ImageView lock;

        public ViewHolder(View view) {
            this.levelNum = (TextView) view.findViewById(R.id.item_level_num);
            this.lock = (ImageView) view.findViewById(R.id.item_lock);
        }
    }

}
