package com.bupc.bupc_quiz;

import com.bupc.bupc_quiz.data.models.Question;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.bupc.bupc_quiz.AppClass.DIFFICULT;
import static com.bupc.bupc_quiz.AppClass.DIFFICULT_LVL_MODE;
import static com.bupc.bupc_quiz.AppClass.EASY;
import static com.bupc.bupc_quiz.AppClass.EASY_LVL_MODE;
import static com.bupc.bupc_quiz.AppClass.MODERATE;
import static com.bupc.bupc_quiz.AppClass.MODERATE_LVL_MODE;

public class DataHolder {

    private final String TAG = DataHolder.class.getSimpleName();

    private List<Question> mAllQuestions = new ArrayList<>();
    private List<Question> mEasyQuestions = new ArrayList<>();
    private List<Question> mModerateQuestions = new ArrayList<>();
    private List<Question> mDifficultQuestions = new ArrayList<>();
    private static final DataHolder mInstance = new DataHolder();
    private int mSelectedDifficulty = 0, mEasyCount = 0, mModerateCount = 0, mDifficultCount = 0, mLevelSelected = 0, mActiveFragment = 0;

    public static synchronized DataHolder getInstance() {
        return mInstance;
    }

    /*
    1 - home/diff selection
    2 - questions
    0 - else
     */
    public void setActiveFragment(int active) {
        mActiveFragment = active;
    }

    public int getActiveFragment() {
        return mActiveFragment;
    }

    public int getCount() {
        return getCount(getSelectedDifficulty());
    }

    public int getCount(int mode) {
        switch (mode) {
            case EASY_LVL_MODE:
                return getEasyCount();
            case MODERATE_LVL_MODE:
                return getModerateCount();
            case DIFFICULT_LVL_MODE:
                return getDifficultCount();
            default:
                return getTotalItems();
        }
    }

    public int getEasyCount() {
        return mEasyCount;
    }

    public void setEasyCount(int mEasyCount) {
        this.mEasyCount = mEasyCount;
    }

    public int getModerateCount() {
        return mModerateCount;
    }

    public void setModerateCount(int mModerateCount) {
        this.mModerateCount = mModerateCount;
    }

    public int getDifficultCount() {
        return mDifficultCount;
    }

    public void setDifficultCount(int mDifficultCount) {
        this.mDifficultCount = mDifficultCount;
    }

    public void setDifficulty(int difficulty) {
        switch (difficulty) {
            case EASY_LVL_MODE:
                mSelectedDifficulty = EASY_LVL_MODE;
                break;
            case MODERATE_LVL_MODE:
                mSelectedDifficulty = MODERATE_LVL_MODE;
                break;
            case DIFFICULT_LVL_MODE:
                mSelectedDifficulty = DIFFICULT_LVL_MODE;
                break;
        }
    }

    public int getSelectedDifficulty() {
        return mSelectedDifficulty;
    }

    public String getSelectedDifficultyString() {
        switch (getSelectedDifficulty()) {
            case EASY_LVL_MODE:
                return EASY;
            case MODERATE_LVL_MODE:
                return MODERATE;
            case DIFFICULT_LVL_MODE:
                return DIFFICULT;
            default:
                return "no";
        }
    }

    public void addQuestion(Question question) {
        if(question != null) {
            switch (question.getDifficulty()) {
                case EASY:
                    mEasyQuestions.add(question);
                    break;
                case MODERATE:
                    mModerateQuestions.add(question);
                    break;
                case DIFFICULT:
                    mDifficultQuestions.add(question);
                    break;
            }
            mAllQuestions.add(question);
        }

    }

    public void addQuestions(List<Question> questions) {
        for (Question question : questions) {
            addQuestion(question);
        }
    }

    public void newQuestions(List<Question> questions) {
        cleanSlateQuestions();
        addQuestions(questions);
    }

    private void cleanSlateQuestions() {
        mAllQuestions.clear();
        mEasyQuestions.clear();
        mModerateQuestions.clear();
        mDifficultQuestions.clear();
    }

    public Question getQuestion(int index) {
        return mAllQuestions.get(index);
    }

    public List<Question> getQuestions() {
        return getQuestions(-1);
    }

    public List<Question> getQuestions(int mode) {
        switch (mode) {
            case EASY_LVL_MODE:
                return mEasyQuestions;
            case MODERATE_LVL_MODE:
                return mModerateQuestions;
            case DIFFICULT_LVL_MODE:
                return mDifficultQuestions;
            default:
                return mAllQuestions;
        }
    }

    public List<Question> getEasyQuestions() {
        return getQuestions(EASY_LVL_MODE);
    }

    public List<Question> getModerateQuestions() {
        return getQuestions(MODERATE_LVL_MODE);
    }

    public List<Question> getDifficultQuestions() {
        return getQuestions(DIFFICULT_LVL_MODE);
    }

    public Question getRandomQuestion() {
        return getRandomQuestion(getSelectedDifficulty());
    }

    public Question getRandomQuestion(int mode) {
        List<Question> leveledQuestions = new ArrayList<>();

        switch (mode) {
            case EASY_LVL_MODE:
                for (Question question : getEasyQuestions()) {
                    if (question.getLevel().equals(String.valueOf(getLevel()))) {
                        leveledQuestions.add(question);
                    }
                }
                break;

            case MODERATE_LVL_MODE:
                for (Question question : getModerateQuestions()) {
                    if (question.getLevel().equals(String.valueOf(getLevel()))) {
                        leveledQuestions.add(question);
                    }
                }
                break;

            case DIFFICULT_LVL_MODE:
                for (Question question : getDifficultQuestions()) {
                    if (question.getLevel().equals(String.valueOf(getLevel()))) {
                        leveledQuestions.add(question);
                    }
                }
                break;
        }

        Collections.shuffle(leveledQuestions);
        return leveledQuestions.get(0);
    }

    public Question getRandomEasyQuestion() {
        Collections.shuffle(mEasyQuestions);
        return mEasyQuestions.get(0);
    }

    public Question getRandomModerateQuestion() {
        Collections.shuffle(mModerateQuestions);
        return mModerateQuestions.get(0);
    }

    public Question getRandomDifficultQuestion() {
        Collections.shuffle(mDifficultQuestions);
        return mDifficultQuestions.get(0);
    }

    public int getTotalItems() {
        return mAllQuestions.size();
    }

    public void setLevel(int i) {
        this.mLevelSelected = i;
    }

    public int getLevel() {
        return mLevelSelected;
    }
}
