package com.bupc.bupc_quiz.fragments;

import android.content.Context;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bupc.bupc_quiz.AppClass;
import com.bupc.bupc_quiz.DataHolder;
import com.bupc.bupc_quiz.DatabaseHandler;
import com.bupc.bupc_quiz.MainActivity;
import com.bupc.bupc_quiz.Prefs;
import com.bupc.bupc_quiz.R;
import com.bupc.bupc_quiz.data.models.Question;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static com.bupc.bupc_quiz.AppClass.DIFFICULT_LVL_MODE;
import static com.bupc.bupc_quiz.AppClass.EASY_LVL_MODE;
import static com.bupc.bupc_quiz.AppClass.MODERATE_LVL_MODE;

public class HomeFragment extends Fragment implements View.OnClickListener {

    private final String TAG = HomeFragment.class.getSimpleName();
    private AppClass mAppClass;
    private Context mContext;
    private DatabaseHandler mDatabaseHandler;
    private DataHolder mDataHolder;
    private View mView;
    private MediaPlayer mStartSound;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_home, container, false);

        mAppClass = AppClass.getInstance();
        mContext = mAppClass.getAppContext();
        mDataHolder = DataHolder.getInstance();

        mDatabaseHandler = new DatabaseHandler(mContext);
        mDatabaseHandler.getReadableDatabase();

//        File database = mContext.getDatabasePath(mDatabaseHandler.DATABASE_NAME);
//        if (false == database.exists()) {
//            mDatabaseHandler.getReadableDatabase();
//        }

        TextView easyDiff = (TextView) mView.findViewById(R.id.diff_easy),
                moderateDiff = (TextView) mView.findViewById(R.id.diff_moderate),
                difficultDiff = (TextView) mView.findViewById(R.id.diff_difficult);

        easyDiff.setOnClickListener(this);
        moderateDiff.setOnClickListener(this);
        difficultDiff.setOnClickListener(this);

        mStartSound = MediaPlayer.create(mContext, R.raw.start);

        return mView;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.diff_easy:
                mDataHolder.setDifficulty(EASY_LVL_MODE);
                break;
            case R.id.diff_moderate:
                mDataHolder.setDifficulty(MODERATE_LVL_MODE);
                break;
            case R.id.diff_difficult:
                mDataHolder.setDifficulty(DIFFICULT_LVL_MODE);
                break;
        }

//        Prefs.setQuestionIndex(0);
//        Prefs.setScore(0);

        if (Prefs.isSoundActive())
            mStartSound.start();

        Fragment frag = new LevelSelectionFragment();
        ((MainActivity) getActivity()).switchFragment(frag);
    }

//    private void setQuestions(int viewId) {
//        List<Question> questions = new ArrayList<>();
//        List<Question> allQuestions = mDatabaseHandler.getQuestions();
//        String difficulty = "";
//
//        switch (viewId) {
//            case R.id.diff_easy:
//                difficulty = "easy";
//                break;
//            case R.id.diff_moderate:
//                difficulty = "moderate";
//                break;
//            case R.id.diff_difficult:
//                difficulty = "difficult";
//                break;
//        }
//
//        for (Question question : allQuestions) {
//            if (question.getDifficulty().equals(difficulty)) {
//                questions.add(question);
//            }
//        }
//
//        DataHolder.getInstance().newQuestions(questions);
//    }
}