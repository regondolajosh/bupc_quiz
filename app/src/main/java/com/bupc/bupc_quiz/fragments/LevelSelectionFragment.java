package com.bupc.bupc_quiz.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.bupc.bupc_quiz.AppClass;
import com.bupc.bupc_quiz.DataHolder;
import com.bupc.bupc_quiz.Prefs;
import com.bupc.bupc_quiz.R;
import com.bupc.bupc_quiz.data.adapter.LevelAdapter;



public class LevelSelectionFragment extends Fragment {

    private final String TAG = LevelSelectionFragment.class.getSimpleName();
    private AppClass mAppClass;
    private Context mContext;
    private DataHolder mDataHolder;
    private ListView mListView;
    private RecyclerView rvListLevels;
    private TextView tvLockedLabel, tvUnlockedLabel;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.fragment_select_level, container, false);
        mAppClass = AppClass.getInstance();
        mContext = mAppClass.getAppContext();
        mDataHolder = DataHolder.getInstance();

        rvListLevels = (RecyclerView) mView.findViewById(R.id.rvLevels);
        tvLockedLabel = (TextView) mView.findViewById(R.id.tvLockedLabel);
        tvUnlockedLabel = (TextView) mView.findViewById(R.id.tvUnlockedLabel);

        int totalLevels = mDataHolder.getCount();
        int levelUnlocked = Prefs.getUnlockedLevel(mDataHolder.getSelectedDifficulty());

        LevelAdapter levelAdapter = new LevelAdapter(getActivity(), levelUnlocked, totalLevels);
        rvListLevels.setAdapter(levelAdapter);
        rvListLevels.setLayoutManager(new LinearLayoutManager(getActivity().getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));

        tvUnlockedLabel.setText(String.valueOf((1 + " - " +levelUnlocked)) + " " + getString(R.string.unlocked));
        tvLockedLabel.setText(String.valueOf(levelUnlocked + 1 + " - " + totalLevels) + " " + getString(R.string.locked));

//        mConnLVAdapter = new ConnLVAdapter(mMainActivity, mContext, mConnections);
//        mConnectionsLV.setAdapter(mConnLVAdapter);
        return mView;
    }
}
