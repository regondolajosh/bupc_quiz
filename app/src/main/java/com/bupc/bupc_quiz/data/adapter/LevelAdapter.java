package com.bupc.bupc_quiz.data.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bupc.bupc_quiz.AppClass;
import com.bupc.bupc_quiz.DataHolder;
import com.bupc.bupc_quiz.MainActivity;
import com.bupc.bupc_quiz.R;
import com.bupc.bupc_quiz.fragments.QuestionsFragment;

public class LevelAdapter extends RecyclerView.Adapter<LevelAdapter.MyViewHolder> {
    public int levelUnlocked;
    public int totalLevels;
    private MainActivity mainActivity;

    public LevelAdapter(Activity activity, int levelUnlocked, int totalLevels) {
        this.mainActivity = (MainActivity) activity;
        this.levelUnlocked = levelUnlocked;
        this.totalLevels = totalLevels;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.level_item, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        holder.tvLevelLabel.setText(String.valueOf(position + 1));
        if ((position+1) <= levelUnlocked) {
            holder.tvLevelLabel.setClickable(true);
            holder.tvLevelLabel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    DataHolder.getInstance().setLevel(position + 1);
                    mainActivity.switchFragment(new QuestionsFragment(), "questions");
                }
            });
        }
        if ((position + 1) > levelUnlocked) {
            holder.ivLock.setVisibility(View.VISIBLE);
            holder.ivLock.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AppClass.getInstance().toaster("This level is locked!", false);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return totalLevels;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView ivLock;
        public TextView tvLevelLabel;

        public MyViewHolder(View itemView) {
            super(itemView);
            ivLock = (ImageView) itemView.findViewById(R.id.item_lock);
            tvLevelLabel = (TextView) itemView.findViewById(R.id.item_level_num);

        }
    }
}
