package com.bupc.bupc_quiz;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.bupc.bupc_quiz.data.models.Question;
import com.bupc.bupc_quiz.fragments.DevelopersFragment;
import com.bupc.bupc_quiz.fragments.HelpFragment;
import com.bupc.bupc_quiz.fragments.HomeFragment;
import com.bupc.bupc_quiz.fragments.LevelSelectionFragment;
import com.bupc.bupc_quiz.fragments.SettingsFragment;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private AppClass mAppClass;
    private Context mContext;
    private DataHolder mDataHolder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mAppClass = AppClass.getInstance();
        mContext = mAppClass.getAppContext();
        mDataHolder = DataHolder.getInstance();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        setupQuestionsFromDB();

        switchFragment(new HomeFragment(), "home");
    }

    private void setupQuestionsFromDB() {
        DatabaseHandler mDatabaseHandler = new DatabaseHandler(mAppClass.getAppContext());
        mDatabaseHandler.initializeDatabase();
        mDatabaseHandler.close();

        mDatabaseHandler = new DatabaseHandler(mContext);
        mDatabaseHandler.getReadableDatabase();

        File database = mContext.getDatabasePath(mDatabaseHandler.DATABASE_NAME);
        if (database.exists()) {
            mDatabaseHandler.getReadableDatabase();
        }

        mDataHolder.newQuestions(mDatabaseHandler.getQuestions());

//        List<Question> questions = new ArrayList<>();
//        List<Question> allQuestions = mDatabaseHandler.getQuestions();
//
//        for (Question question : allQuestions) {
//            if (question.getDifficulty().equals("easy")) {
//                questions.add(question);
//            }
//        }
    }

//    @Override
//    public void onBackPressed() {
//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        if (drawer.isDrawerOpen(GravityCompat.START)) {
//            drawer.closeDrawer(GravityCompat.START);
//        } else {
//            super.onBackPressed();
//        }
//    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);

        } else {
            switch (mDataHolder.getActiveFragment()) {
                case 1:
                    new AlertDialog.Builder(this).setIcon(android.R.drawable.ic_dialog_alert).setTitle("Exit")
                            .setMessage("Are you sure?")
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    finish();
                                }
                            }).setNegativeButton("No", null).show();
                    break;
                case 2:
                    switchFragment(new LevelSelectionFragment());
                    break;
                default:
                    switchFragment(new HomeFragment(), "home");
                    break;
            }
        }
    }


//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.main, menu);
//        return true;
//    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Fragment frag = null;
        String tag = "";

        if (id == R.id.nav_home) {
            frag = new HomeFragment();
            tag = "home";
        } else if (id == R.id.nav_help) {
            frag = new HelpFragment();
        } else if (id == R.id.nav_settings) {
            frag = new SettingsFragment();
        } else if (id == R.id.nav_devs) {
            frag = new DevelopersFragment();
        }

        switchFragment(frag, tag);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void switchFragment(Fragment fragment) {
        switchFragment(fragment, "");
    }

    public void switchFragment(Fragment fragment, String tag) {
        switch (tag) {
            case "home":
                mDataHolder.setActiveFragment(1);
                break;
            case "questions":
                mDataHolder.setActiveFragment(2);
                break;
            default:
                mDataHolder.setActiveFragment(0);
                break;
        }

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.abc_fade_in, R.anim.abc_fade_out);
        ft.replace(R.id.container, fragment, tag);
        ft.addToBackStack(null);
        ft.commit();
//        ft.commitAllowingStateLoss();
    }
}
