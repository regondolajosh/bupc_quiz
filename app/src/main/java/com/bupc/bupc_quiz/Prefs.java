package com.bupc.bupc_quiz;

import android.content.Context;
import android.content.SharedPreferences;

import static com.bupc.bupc_quiz.AppClass.DIFFICULT_LVL_MODE;
import static com.bupc.bupc_quiz.AppClass.EASY_LVL_MODE;
import static com.bupc.bupc_quiz.AppClass.MODERATE_LVL_MODE;

public class Prefs {

    private static final String SHARED_PREFS = "bupc_quiz_prefs";
    private static final String PREFS_SETTINGS_SOUND = "settings_sound";
    private static final String PREFS_QUIZ_DIFFICULTY = "quiz_difficulty";
    private static final String PREFS_QUESTION_INDEX = "question_num";
    private static final String PREFS_TOTAL_ITEMS = "total_items";
    private static final String PREFS_LEVEL_SELECTED = "level_selected";
    private static final String PREFS_SCORE = "score";
    private static final String PREFS_EASY_LVL = "easy_lvl";
    private static final String PREFS_MODERATE_LVL = "moderate_lvl";
    private static final String PREFS_DIFFICULT_LVL = "difficult_lvl";


    private static final SharedPreferences prefs =
            AppClass.getInstance().getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE);
    private static final SharedPreferences.Editor editor = prefs.edit();

    public static void setSoundStatus(boolean active) {
        editor.putBoolean(PREFS_SETTINGS_SOUND, active);
        editor.commit();
    }

    public static boolean isSoundActive() {
        return prefs.getBoolean(PREFS_SETTINGS_SOUND, false);
    }

//    public static void setQuestionIndex(int diff) {
//        editor.putInt(PREFS_QUESTION_INDEX, diff);
//        editor.commit();
//    }
//
//    public static int getQuestionIndex() {
//        return prefs.getInt(PREFS_QUESTION_INDEX, 0);
//    }

    public static void setScore(int score) {
        editor.putInt(PREFS_SCORE, score);
        editor.commit();
    }

    public static int getScore() {
        return prefs.getInt(PREFS_SCORE, 0);
    }

//    public static void setLevel(int level) {
//        editor.putInt(PREFS_LEVEL_SELECTED, level);
//        editor.commit();
//    }
//
//    public static int getLevel() {
//        return prefs.getInt(PREFS_LEVEL_SELECTED, 0);
//    }

    public static void unlockNextLevel() {
        DataHolder dataHolder = DataHolder.getInstance();
        if (dataHolder.getLevel() == getUnlockedLevel()) {
            int nextLevel = dataHolder.getLevel() + 1;
            AppClass.getInstance().toaster("Level " + nextLevel + " unlocked!");
            setUnlockedLevel(dataHolder.getSelectedDifficulty(), nextLevel);
        }
    }

    public static void setUnlockedLevel(int difficulty, int level) {
        switch (difficulty) {
            case EASY_LVL_MODE:
                editor.putInt(PREFS_EASY_LVL, level);
                break;
            case MODERATE_LVL_MODE:
                editor.putInt(PREFS_MODERATE_LVL, level);
                break;
            case DIFFICULT_LVL_MODE:
                editor.putInt(PREFS_DIFFICULT_LVL, level);
                break;
        }
        editor.commit();
    }

    public static int getUnlockedLevel() {
        return getUnlockedLevel(DataHolder.getInstance().getSelectedDifficulty());
    }

    public static int getUnlockedLevel(int difficulty) {
        switch (difficulty) {
            case EASY_LVL_MODE:
                return prefs.getInt(PREFS_EASY_LVL, 2);
            case MODERATE_LVL_MODE:
                return prefs.getInt(PREFS_MODERATE_LVL, 2);
            case DIFFICULT_LVL_MODE:
                return prefs.getInt(PREFS_DIFFICULT_LVL, 2);
            default:
                return 0;
        }
    }
}
